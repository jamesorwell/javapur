/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pur;

/**
 *
 * @author ku14009
 */
public class PurCalculator {

    public class Result {

        public double pur;
        public double priorEntropy;
        public double posteriorEntropy;
        
        public String toString() {
            return priorEntropy + "\t" + posteriorEntropy + "\t" + pur;
        }
    }
    public Result getPur(double[][] predictions, int[] labels) {
        int numLabels = predictions[0].length;
        double priorEntropy = getPriorEntropy(labels, numLabels);
        double posteriorEntropy = getPosteriorEntropy(predictions, labels);
        return getPur(priorEntropy, posteriorEntropy);           
    }
    public Result getPur( double priorEntropy, double posteriorEntropy)
    {
        Result r = new Result();
        r.priorEntropy = priorEntropy;
        r.posteriorEntropy = posteriorEntropy;
        r.pur = (priorEntropy-posteriorEntropy)/priorEntropy;
        return r;
    }
    public double getPriorEntropy(int[] labels, int numLabels) {
        int hist[] = new int[numLabels];        
        double sum = 0.0f;
        int numSamples = labels.length;
        for(int i = 0;i<numSamples;i++) {
            hist[labels[i]]++;
        }
        for(int i = 0;i<numLabels;i++) {
            if(hist[i]>0) {
                double p = ((double)hist[i])/(double)numSamples;
                sum -= p*Math.log(p); 
            }
        }
        return sum/Math.log(2);
    }
    public double getPosteriorEntropy(double[][] predictions, int[] labels)
    {
        int numSamples = predictions.length;
        double log2 = Math.log(2);
        double sumLogP = 0.0;
        for (int i = 0; i < numSamples; i++) {
            int labelIndex = labels[i];
            double logp = Math.log(predictions[i][labelIndex]);
            sumLogP -= logp/log2;
        }
        double posteriorEntropy = sumLogP/numSamples;
        return posteriorEntropy;
    }
}
