/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pur;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ku14009
 */
public class Test4EqualPriorGreenBottleSideOptimal {

    /* the test cases:
    1) testEqualPriorNoSideOptimal
    2) testEqualPriorNoSideSuboptimal //varying degree of incline and power of noise (Crazy Horse), 
    3) testUnequalPriorNoSideOptimal  // known distribution of relative frequency to model, e.g. exponential 
    4) testEqualPriorGreenBottleSideOptimal // removing 1 bit of uncertainty at a time
    5) testEqualPriorNoisySideOptimal // set power ratio of 'side signal' to noise ratio
    6) testEqualPriorNoisySideSuboptimal
    7) testUnequalPriorNoisySideOptimal
    8) testEqualPriorDropOutSideOptimal
    */
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPur method, of class PurCalculator.
     */
    @Test
    public void testGetPurTenGreenBottles() {
        Random random = new Random();
        System.out.println("ten Green Bottles:");
        int numSamples = 100000;
        int numGreenBottles = 10;
        int numLabels = (int) Math.pow(2, numGreenBottles);
        int labels[] = new int[numSamples];
        for (int i = 0; i < numSamples; i++) {
            labels[i] = random.nextInt(numLabels);
        }
        PurCalculator purCalc = new PurCalculator();
        for (int thisGreenBottle = numGreenBottles; thisGreenBottle > 0; thisGreenBottle--) {
            int numPossibleLabels = (int) Math.pow(2, thisGreenBottle);
            double predictions[][] = getPredictions(labels, numLabels, numPossibleLabels);
            PurCalculator.Result result = purCalc.getPur(predictions, labels);
            System.out.println("prior is " + result.priorEntropy + "and posterior is " + result.posteriorEntropy);
            assertEquals((double) (numGreenBottles - thisGreenBottle) / numGreenBottles, result.pur, 0.005);
        }

    }

    
    private double[][] getPredictions(int[] labels, int numLabels, int numPossibleLabels) {
        double[][] predictions = new double[labels.length][numLabels];
        double[] seed = new double[numLabels];
        for (int i = 0; i < numPossibleLabels; i++) {
            seed[i] = 1.0 / (double) (numPossibleLabels);
        }
        for (int thisSample = 0; thisSample < labels.length; thisSample++) {
            shuffle(seed);
            int thisLabel = labels[thisSample];
            if (seed[thisLabel] == 0) {
                seed[thisLabel] = 1.0 / (double) (numPossibleLabels);
                zeroNext(seed, thisLabel);
            }
            System.arraycopy(seed, 0, predictions[thisSample], 0, numLabels);
       }
        int errorIndex = check(predictions, labels);
        if(errorIndex>=0)
        {
            System.out.println("error in predictions at line " +errorIndex);
            return null;
        }
        return predictions;
    }

    private void shuffle(double[] ar) {
        {
            // If running on Java 6 or older, use `new Random()` on RHS here
            Random rnd = ThreadLocalRandom.current();
            for (int i = ar.length - 1; i > 0; i--) {
                int index = rnd.nextInt(i + 1);
                // Simple swap
                double a = ar[index];
                ar[index] = ar[i];
                ar[i] = a;
            }
        }
    }

    private void zeroNext(double[] seed, int thisLabel) {
        int numLabels = seed.length;
        int nextIndex = thisLabel+1;
        boolean done = false;
        while(!done)
        {
            if(nextIndex>=numLabels) {
                nextIndex =0;
            }
            if(seed[nextIndex]>0) {
                seed[nextIndex]=0;
                done = true;
            }
            nextIndex++;
        }
    }

    private int check(double[][] predictions, int[] labels) {
        int numSamples = predictions.length;
        int numLabels = predictions[0].length;
        for(int i = 0;i<numSamples;i++) {
            
            int labelIndex = labels[i];
            if(labelIndex>=numLabels) {
                return i;
            }
            if(predictions[i][labelIndex]<0.000000001) {
                return i;
            }
            double sum = 0;
            
            for(int j = 0;j<numLabels;j++) {
                sum += predictions[i][j];
                
            }
            if(Math.abs(sum-1)>0.00001) {
                return i;
            }
        }
        return -1;
    }
}