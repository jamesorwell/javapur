package ku.pur;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ku14009
 */
public class SamplesFactory {

    static double[][] getPredictions(int numSamples, int numLabels) {
        double[][] predictions = new double[numSamples][numLabels];
        double prior = 1.0/(double)numLabels;
        for(int thisSample =0;thisSample<numSamples;thisSample++) {
            double sum = 0;
            for (int thisLabel=0;thisLabel<numLabels;thisLabel++) {
                predictions[thisSample][thisLabel] = prior;
            }
        }
        return predictions;
    }    
    
    static double[][] getNoisyPredictions(int numSamples, int numLabels, double noisePower) {
        double[][] predictions = new double[numSamples][numLabels];
        double prior = 1.0/(double)numLabels;
        Random R = new Random();
        for(int thisSample =0;thisSample<numSamples;thisSample++) {
            double sum = 0;
            for (int thisLabel=0;thisLabel<numLabels;thisLabel++) {
                double r = R.nextGaussian();
                predictions[thisSample][thisLabel] = (1-noisePower)*prior + 
                                                    noisePower*R.nextDouble();
                sum += predictions[thisSample][thisLabel];
            }
            for (int thisLabel=0;thisLabel<numLabels;thisLabel++) {
                predictions[thisSample][thisLabel]/=sum;
            }
        }
        return predictions;
    }
    
    

    static int[] getLabels(int numSamples, int numLabels) {
        int labels[] = new int[numSamples];
        Random r = new Random();
        for(int i = 0;i<numSamples;i++) {
            labels[i] = r.nextInt(numLabels);
        }
        return labels;
    }
    
}
