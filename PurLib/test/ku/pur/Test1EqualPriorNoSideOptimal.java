/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pur;

/**
 *
 * @author ku14009
 */
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;



public class Test1EqualPriorNoSideOptimal {

    @Test
    public void testGetPurTwoPowerNLabels () {
        int numSamples = 100000;
        PurCalculator purCalc = new PurCalculator();
        System.out.println("testing Equal Prior, Optimal bets, variation in numLabels");
        System.out.println("n\tprior\tpost\tpur\n");
        for(int n = 1;n<10;n++)
        {
            int numLabels = (int)(Math.pow(2, n));
            int[] labels = SamplesFactory.getLabels(numSamples, numLabels);
            double[][] predictions = SamplesFactory.getPredictions(numSamples, numLabels);
            PurCalculator.Result result = purCalc.getPur(predictions, labels);
            System.out.println(numLabels + "\t" + result.toString());
            assertEquals((double)(n), result.priorEntropy, 0.005);
            assertEquals((double)(n), result.posteriorEntropy, 0.005);
            assertEquals(0, result.pur, 0.005);
            
        }
        
    }
}
