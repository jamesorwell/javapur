/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pur;

import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ku14009
 */
public class Test2EqualPriorNoSideSuboptimal {
    
    public Test2EqualPriorNoSideSuboptimal() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPur method, of class PurCalculator.
     */
    @Test
    public void testGetPurTwoLabelsIncreasingNoise() {
        System.out.println("getPur");
        int numSamples = 100000;
        int numLabels = 2;
        
        int[] labels = SamplesFactory.getLabels(numSamples, numLabels);
        PurCalculator purCalc = new PurCalculator();
        
        for(double noiseFactor = 0;noiseFactor<=1.0;noiseFactor += 0.1) {                   
            
            double [][] predictions = SamplesFactory.getNoisyPredictions(numSamples, numLabels, noiseFactor);
            PurCalculator.Result result = purCalc.getPur(predictions, labels);
            System.out.println(noiseFactor + "\t" + result.toString());
        }    
    }
 }
